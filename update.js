const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");

var app = Express();
Mongoose.connect("mongodb://localhost/employe", { useNewUrlParser: true });
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
const port = process.env.PORT || '5000';
app.set('port', port);
const PersonModel = Mongoose.model("person",{
    name: String,
    email: String,
    mobileno: String
});
app.put("/person/:id", async (request, response) => {
    try {
        console.log("adadasdasdas",req.params.id);
        var person = await PersonModel.findById(request.params.id).exec();
        person.set(request.body);
        var result = await person.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});
app.listen(port, () => {
    console.log("Listening at :" + port);
});
module.exports = app;