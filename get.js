const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");

var app = Express();
Mongoose.connect("mongodb://localhost/employe", { useNewUrlParser: true });
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
const port = process.env.PORT || '5000';
app.set('port', port);
const PersonModel = Mongoose.model("person",{
    name: String,
    email: String,
    mobileno: String
});
app.get("/people", async (request, response) => {
    try {
        var result = await PersonModel.find().exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

app.listen(port, () => {
    console.log("Listening at :" + port);
});
module.exports = app;