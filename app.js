const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");
var app = Express();


Mongoose.connect("mongodb://localhost/employe", { useNewUrlParser: true });
Mongoose.set('useCreateIndex', true);

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
const port = process.env.PORT || '5000';
app.set('port', port);
const PersonModel = Mongoose.model("person",{
    name: { type:String ,required:true},
    email:{ type: String, unique: true}, 
    mobileno:{ type: String, minlength:10,maxlength:13}
});

app.post("/person", async (request, response) => {
    try {
        var person = new PersonModel(request.body);
        var result = await person.save();
        response.send(result);
    } 
    catch (error)
     {
        response.status(500).send(error);
      }
});

app.listen(port, () => {
    console.log("Listening at :" + port );
});
module.exports = app;