'use strict';
const chai = require('chai');
const expect = require('chai').expect;
chai.use(require('chai-http'));
const app = require('/home/ubuntu/3/app'); 
var sinon = require('sinon');

describe('POST /person', function(){
  
  it('should add new employe databse', function() {
    return chai.request(app)
      .post('/person')
      .send({
        name: 'xyzwww',
        email: 'xywwwwz@gmail.com',
        mobileno: '12347856789'
      })
      .then(function(res) {
       
       //console.log(res);
        expect(res).to.have.status(500);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.name).to.be.an('string');
        expect(res).to.be.not.empty;
        expect(res.body).to.have.property('name');
       
      });
  });
  it('shoul data is being saved into datbase', function(){
     return chai.request(app)
     .post('/person')
     .send({
      name: 'xyzwww',
      email: 'xywwwwz@gmail.com',
      mobileno: '12347856789'
     }).then(function(res){
        console.log(res);
     });



  });
 it('should return Bad Request', function() {
    return chai.request(app)
      .post('/person/notfound')      
      .send({
        name: 'xyz1',
        email: 'xyz1@gmail.com',
        mobileno: '1234567891'
      })
      .then(function(res) {
        expect(404)
      })
     
  });
  
});


